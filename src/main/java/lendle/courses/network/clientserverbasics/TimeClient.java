/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lendle.courses.network.clientserverbasics;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 *
 * @author lendle
 */
public class TimeClient {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception{
        // TODO code application logic here
        // hint: 建立 Socket 物件，並且取得 input stream 儲存在 input 變數中
        Socket socket = null;
        InputStream input = null;
        socket = new Socket("time.nist.gov", 13); // 只是時間，都是 ASCII
        input = socket.getInputStream(); // 以 byte 在操作
        
        StringBuilder builder = new StringBuilder(); 
        // StringBuilder 上的主要操作是 append 和 insert 方法
        // 每个都有效地將給定的數據轉換為字串，然後將該字串的字符追加或插入到字串建構器中ㄋ。
        
        InputStreamReader reader = new InputStreamReader(input); // 第二個參數可以放編碼方式
        for(int c=reader.read(); c!=-1; c=reader.read()){
            builder.append((char)c);
        }
        System.out.println(builder);
        socket.close();
    }
    
}
