/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lendle.courses.network.clientserverbasics;

import java.net.Socket;

/**
 *
 * @author lendle
 */
public class PortScanner {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args){
        // TODO code application logic here
        for(int i=0; i<=1024; i++){
            // hint: 建立 socket，連結到 i port
            try ( Socket socket = new Socket("localhost", i) ) { // 大括弧內執行完就會關閉
            // ifconfig en0 可以看自己的 ip 位址（windows: ipconfig）
            /////////////////////////////////
                System.out.println("there is a service on port: " + i); 
            } catch ( Exception e ) { // 連不上就會跑出 exception
                // 什麼都不會發生
                System.out.println("\t there is no service on " + i); // \t 是 tab
                
            }
        }
    }
    
}
