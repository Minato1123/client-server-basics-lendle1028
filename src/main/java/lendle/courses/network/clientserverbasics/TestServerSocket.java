/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package lendle.courses.network.clientserverbasics;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author carrie
 */
public class TestServerSocket {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        // TODO code application logic here
        ServerSocket serverSocket = new ServerSocket(8888);
        // 實作一個伺服器，可以使用 ServerSocket Class，它包括了伺服器傾聽與客戶端連線的方法
        // 傾聽連線或關閉連線時，可以使用 accept() 與 close() 方法
        
        while (true) { // 無窮迴圈等待使用者的輸入
            Socket client = serverSocket.accept(); // 回傳 socket
            // accept() 傳回的是有關連線客戶端的Socket物件資訊，可以用它來取得客戶端的連線資訊，或關閉客戶端的連線。
            
            OutputStream outputStream = client.getOutputStream();
            OutputStreamWriter writer = new OutputStreamWriter(outputStream, "utf-8"); // 如果下一行要輸入中文，第二參數要加上 utf-8
            writer.write("HTTP/1.0 200 OK\r\n\r\n"); // 200 代表沒事
//            writer.write("<h1>Hello World!</h1>");
            writer.write("<html><head><meta charset='utf-8'></head>");
            writer.write("<p>你好我是醜醜文字</p>");
            writer.write("</html>");
            writer.flush();
            client.close(); // 關閉此 Socket，上面兩個會自動關閉
            Thread.sleep(30000); // sleep 30s
        }
    }
    
}
