/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package lendle.courses.network.clientserverbasics;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;

/**
 *
 * @author carrie
 */
public class DictClient1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("dict.org", 2628);
        InputStream input = socket.getInputStream(); // byte stream
        OutputStream output = socket.getOutputStream(); // byte stream
        OutputStreamWriter writer = new OutputStreamWriter(output); // ASCII 可以不用寫第二參數
        InputStreamReader inputStreamReader = new InputStreamReader(input);
        writer.write("DEFINE wn network\r\n"); // 寫入指令（利用 writer 做輸出）
        writer.flush(); // 把 buffer 清除（如果沒清，上面指令有可能沒送出去）
        
        // 讀資料
        // inputStreamReader.read(); // 每次讀進來是一個字元
        BufferedReader reader = new BufferedReader(inputStreamReader); // 每次讀一行
        for (String line = reader.readLine(); !line.equals("."); line = reader.readLine()) {
            System.out.println(line);
        }
        writer.write("quit\r\n");
        socket.close();
        
    }
    
}
