/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lendle.courses.network.clientserverbasics;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.Socket;

/**
 *
 * @author lendle
 */
public class DictClient {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception { // 字典服務
        // TODO code application logic here
        Socket socket = new Socket("dict.org", 2628); // 建立 Socket 做連線（主機、port number）
        OutputStream out = socket.getOutputStream(); // 輸出串流，處理 byte
        Writer writer = new OutputStreamWriter(out, "UTF-8");
        writer.write("DEFINE wn dog\r\n"); // 字典服務的指令，wn 後面是要查詢的字，\r\n 是 Enter
        writer.flush(); // 確保前面的輸入有進去
        InputStream in = socket.getInputStream(); // 輸入串流
        // 輸入串流建立緩衝區物件
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(in, "UTF-8"));
        // BufferedReader 在建構時接受一個 Reader 物件
        // 在讀取標準輸入串流時，會使用 InputStreamReader 它繼承了 Reader 類別
        
        // 這個字典的最後是一個點結尾（所以用找到點為止來當作結束）
        for (String line = reader.readLine(); !line.equals("."); line = reader.readLine()) {
            System.out.println(line);
        }
        writer.write("quit\r\n");
        writer.flush();
        socket.close(); // 把 Socket 連線關掉
    }

}
